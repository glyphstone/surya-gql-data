import { DataSource } from "apollo-datasource"

export class BaseAPI extends DataSource {
  constructor(container, type, typeMap, config) {
    super()
    this.container = container
    this.log = container.log
    this.db = container.db
    this.type = type
    this.typeMap = typeMap
    this.config = config
    this.context = null
  }

  /**
   * This is a function that gets called by ApolloServer when being setup.
   */
  initialize(config) {
    this.context = config.context
  }

  getTypeName() {
    return this.type.getName()
  }

  getType() {
    return this.type
  }
}
