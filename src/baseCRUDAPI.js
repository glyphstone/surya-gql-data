import { BaseAPI } from "./baseAPI"

const DEFAULT_LIMIT = 100

export class BaseCRUDAPI extends BaseAPI {
  constructor(container, type, typeMap, config) {
    super(container, type, typeMap, config)
  }

  async query(
    offset = 0,
    limit = DEFAULT_LIMIT,
    filter = "",
    sort = "",
    session
  ) {
    console.log(`Not implemented`)
    throw new Error("Not Implemented")
    return {}
  }

  async get(id, session) {
    console.log(`Not implemented`)
    throw new Error("Not Implemented")
    return {}
  }

  async create(obj, session) {
    console.log(`Not implemented`)
    throw new Error("Not Implemented")
    return {}
  }

  async update(id, obj, session) {
    console.log(`Not implemented`)
    throw new Error("Not Implemented")
    return {}
  }

  async delete(id, session) {
    console.log(`Not implemented`)
    throw new Error("Not Implemented")
    return {}
  }
}
