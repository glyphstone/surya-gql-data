const sortGrammar = String.raw`

    //special ignore token to allow and ignore whitespace
    ignore = ~[\s]+~ ;

    comma = ~,~ ;
    asc = ~(ASC|asc)~ ;
    desc = ~(DESC|desc)~ ;
    lParen = ~\(~ ;
    rParen = ~\)~ ;
    trueLiteral = ~true~ ;
    falseLiteral = ~false~ ;

    identifier = ~[a-zA-Z_][0-9a-zA-Z_\-\.]{0,100}~ ;
    stringLiteral = ~'([^'\\]*(?:\\.[^'\\]*)*)'~ ;
    number = ~(\-)?[0-9]+(\.[0-9]+)?~ ;


    program : sortSpecList ;

    sortSpecList : sortSpec < ( < comma sortSpec ) = moresort * ;
    sortSpec : field < ( asc | desc ) = asc_desc ? ;

    field:
        < paramField |
        < simpleField  ;

    paramField: @ identifier paramList ;
    simpleField: @ identifier ;

    paramList: <lParen < literalList < rParen ;

    literalList : < literal < ( < comma < literal ) = moreliterals *  ;

    literal : stringLiteral | number | < booleanLiteral ;

    booleanLiteral: trueLiteral | falseLiteral ;

    `
module.exports = sortGrammar
